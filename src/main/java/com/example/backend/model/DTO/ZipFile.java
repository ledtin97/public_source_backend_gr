/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.backend.model.DTO;

import java.util.List;

/**
 *
 * @author LEDTIN
 */
public class ZipFile {
    private List<String> list;

    public ZipFile() {
    }

    public ZipFile(List<String> list) {
        this.list = list;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
    
    
}
