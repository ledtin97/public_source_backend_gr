package com.example.backend.model.DTO;

/**
 *
 * @author LEDTIN
 */
public class FaculityReport {
    private int id;
    private String name;
    private int contributions;
    private int contributors;

    public FaculityReport() {
    }

    public FaculityReport(int id, String name, int contributions, int contributors) {
        this.id = id;
        this.name = name;
        this.contributions = contributions;
        this.contributors = contributors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

    public int getContributors() {
        return contributors;
    }

    public void setContributors(int contributors) {
        this.contributors = contributors;
    }
    
    
}
