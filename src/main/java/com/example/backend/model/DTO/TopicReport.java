package com.example.backend.model.DTO;

import java.util.List;

/**
 *
 * @author LEDTIN
 */
public class TopicReport {
    private int id;
    private String name;
    private List<FaculityReport> list;

    public TopicReport() {
    }

    public TopicReport(int id, String name, List<FaculityReport> list) {
        this.id = id;
        this.name = name;
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FaculityReport> getList() {
        return list;
    }

    public void setList(List<FaculityReport> list) {
        this.list = list;
    }
    
    
}
