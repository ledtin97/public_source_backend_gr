package com.example.backend.model.DTO;

/**
 *
 * @author LEDTIN
 */
public class UserReport {
    private int id;
    private String email;
    private int acticalNumbers;

    public UserReport() {
    }

    public UserReport(int id, String email, int acticalNumbers) {
        this.id = id;
        this.email = email;
        this.acticalNumbers = acticalNumbers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getActicalNumbers() {
        return acticalNumbers;
    }

    public void setActicalNumbers(int acticalNumbers) {
        this.acticalNumbers = acticalNumbers;
    }
    
    
}
