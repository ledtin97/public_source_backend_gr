package com.example.backend.controller;

import com.example.backend.model.Actical;
import com.example.backend.model.Comment;
import com.example.backend.model.DTO.FaculityReport;
import com.example.backend.model.DTO.TopicReport;
import com.example.backend.model.DTO.UserReport;
import com.example.backend.model.DTO.ZipFile;
import com.example.backend.model.Faculity;
import com.example.backend.model.Topic;
import com.example.backend.model.UserInfo;
import com.example.backend.repository.GenericRepository;
import com.example.backend.utils.ResponseUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author LEDTIN
 */
@CrossOrigin(origins = "*")
@RestController
public class ReportController {

    private final ResponseUtils res = new ResponseUtils();

    @Autowired
    private GenericRepository genericRepository;

    @PostMapping("/fileZip")
    public ObjectNode getZipFile(@RequestBody ZipFile listZipFile) {
        try {

            try {
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
                LocalDateTime now = LocalDateTime.now();
                String fileZip = (dtf.format(now) + ".zip");
                File file = new File(fileZip);

                FileOutputStream fos = new FileOutputStream(file);
                ZipOutputStream zos = new ZipOutputStream(fos);

                for (int i = 0; i < listZipFile.getList().size(); i++) {
                    addToZipFile(listZipFile.getList().get(i), zos);
                }

                zos.close();
                fos.close();

                return res.mySuccessJSON("http://tinled.timhsoft.tk/" + fileZip);
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
                return res.myErrorJSON(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
                return res.myErrorJSON(e.getMessage());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return res.myErrorJSON(e.getMessage());
        }
    }

    private void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

        System.out.println("Writing '" + fileName + "' to zip file");

        URL url = new URL(fileName);
        String[] listString = url.getFile().split("/");
        String zipEntryName = listString[listString.length - 1];

        try (InputStream fis = url.openStream()) {

            ZipEntry zipEntry = new ZipEntry(zipEntryName);
            zos.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }

            zos.closeEntry();
        }
    }

    @GetMapping("/report")
    public ObjectNode getReport() {

        try {
            List<Topic> listTopic = genericRepository.findAll(Topic.class).collect(Collectors.toList());
            List<TopicReport> listRS = new ArrayList<>();

            for (int i = 0; i < listTopic.size(); i++) {
                TopicReport tRp = new TopicReport(listTopic.get(i).getId(), listTopic.get(i).getName(), staticListFaculityReport(listTopic.get(i).getId()));
                listRS.add(tRp);
            }

            return res.myJSON(listRS, listRS.size());
        } catch (Exception e) {
            return res.myErrorJSON(e.getMessage());
        }

    }

    private List<FaculityReport> staticListFaculityReport(Integer topicId) {
        List<Actical> listBaseTopicId = genericRepository
                .query(Actical.class)
                .where()
                .eq("topic_id", topicId)
                .findList();

        List<Faculity> listFaculity = genericRepository.findAll(Faculity.class).collect(Collectors.toList());

        List<FaculityReport> listRS = new ArrayList<>();

        for (int i = 0; i < listFaculity.size(); i++) {
            int contributions = 0;

            for (int j = 0; j < listBaseTopicId.size(); j++) {
                if (listBaseTopicId.get(j).getUserInfo().getFaculity().getId() == listFaculity.get(i).getId()) {
                    contributions += 1;
                }
            }

            int contributors = staticReportContributorsByFaculity(listFaculity.get(i).getId(), listBaseTopicId);

            FaculityReport rp = new FaculityReport(listFaculity.get(i).getId(), listFaculity.get(i).getName(), contributions, contributors);
            listRS.add(rp);
        }

        return listRS;
    }

    private Integer staticReportContributorsByFaculity(Integer faculityId, List<Actical> listBaseTopicId) {
        int contributorsOfFaculity = 0;

        List<UserInfo> listUserOfFaculity = genericRepository
                .query(UserInfo.class)
                .where()
                .eq("faculity_id", faculityId)
                .findList();

        for (int i = 0; i < listUserOfFaculity.size(); i++) {
            UserReport user = new UserReport(listUserOfFaculity.get(i).getId(), listUserOfFaculity.get(i).getEmail(), 0);

            for (int j = 0; j < listBaseTopicId.size(); j++) {
                if (listBaseTopicId.get(j).getUserInfo().getId() == user.getId()) {
                    user.setActicalNumbers(user.getActicalNumbers() + 1);
                }
            }

            if (user.getActicalNumbers() > 0) {
                contributorsOfFaculity += 1;
            }
        }

        return contributorsOfFaculity;
    }
}
