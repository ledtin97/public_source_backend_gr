package com.example.backend;

import com.example.backend.config.ConfigInfo;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = {"com.example"})
public class BackendApplication {

    private static final String TAG = BackendApplication.class.getName();
    private static final Logger LOGGER = LogManager.getRootLogger();

    public static void main(String[] args) {

	    System.err.println("==============>" + ConfigInfo.SPRING_BOOT_CONFIG);
	    ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(BackendApplication.class)
		    .properties("spring.config.location:" + ConfigInfo.SPRING_BOOT_CONFIG)
		    .build().run(args);
    }
}
